let ProductList = [];
const BASE_URL = "https://63e49a584474903105ede9f8.mockapi.io";
let dsspjson = localStorage.getItem("cart");
function displayProductList(products) {
  let html = "";
  products.forEach(function (product) {
    html += `<tr>
                  <td>${product.id}</td>
                  <td>${product.name}</td>
                  <td>${product.price}</td>
                  <td>${product.screen}</td>
                  <td>${product.backCamera}</td>
                  <td>${product.frontCamera}</td>
                  <td><img src="${product.img}" width="70px" height="70px"></td>
                  <td>${product.desc}</td>
                  <td>${product.type}</td>
                  <button onclick="addToCart('${product.id}')" class="btn btn-success">Thêm vào giỏ hàng</button>
              </tr>`;
  });
  document.getElementById("tbodysanpham").innerHTML = html;
}

function fetchProductList() {

  axios({
    url: `${BASE_URL}/Product`,
    method: "GET",
  })
    .then(function (response) {
      ProductList = response.data;
      displayProductList(ProductList);
    })
    .catch(function (error) {
      console.log(error);
    });
}

fetchProductList();

document
  .getElementById("selectType")
  .addEventListener("change", filterProductList);

function filterProductList() {
  let type = document.getElementById("selectType").value;
  let filteredProductList = [];
  if (type === "") {
    filteredProductList = ProductList;
  } else {
    for (let i = 0; i < ProductList.length; i++) {
      if (ProductList[i].type === type) {
        filteredProductList.push(ProductList[i]);
      }
    }
  }
  displayProductList(filteredProductList);
}

let cart = [];

function addToCart(id) {
  let product = ProductList.find((product) => product.id === id);
  if (!product) {
    console.error(`Sản phẩm có id = ${id} không tồn tại`);
    return;
  }

  let cartItem = {
    product: product,
    quantity: 1,
  };

  // Tìm xem sản phẩm có trong giỏ hàng chưa
  let found = false;
  for (let i = 0; i < cart.length; i++) {
    if (cart[i].product.id === product.id) {
      // Nếu sản phẩm đã tồn tại trong giỏ hàng, tăng số lượng
      cart[i].quantity++;
      found = true;
      break;
    }
  }

  if (!found) {
    // Nếu sản phẩm chưa tồn tại trong giỏ hàng, thêm mới sản phẩm với số lượng là 1
    cart.push(cartItem);
  }

  // Hiển thị thông báo đã thêm sản phẩm vào giỏ hàng
  alert(`Đã thêm ${product.name} vào giỏ hàng`);
  localStorage.setItem('cart', JSON.stringify(cart));
  renderCart();
}

function renderCart() {
  let table =
    "<table><tr><th>Name</th><th>Price</th><th>Quantity</th><th>Total</th><th></th></tr>";
  let total = 0;
  for (let i = 0; i < cart.length; i++) {
    total += cart[i].product.price * cart[i].quantity;
    table +=
      "<tr><td>" +
      cart[i].product.name +
      "</td><td>" +
      cart[i].product.price +
      "</td><td>" +
      cart[i].quantity +
      "</td><td>" +
      cart[i].product.price * cart[i].quantity +
      "</td></tr>";
  }
  table += "</table>";
  document.getElementById("cart").innerHTML = table;
  document.getElementById("total").innerHTML = "Total: " + total;
}


