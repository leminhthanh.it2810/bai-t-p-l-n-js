var BASE_URL = "https://63e49a584474903105ede9f8.mockapi.io/Product";
function apiGetProducts(search) {
    return axios({
        url: BASE_URL,
        method: "GET",
        params:{
            name: search,
        },
    });
}
function apiAddProduct(product) {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: product,  
    });
  }
  function apiDeleteProduct(productId) {
    return axios({
      url: `${BASE_URL}/${productId}`,
      method: "DELETE",
    });
  }
  function apiGetProductDetail(productId) {
    return axios({
      url: `${BASE_URL}/${productId}`,
      method: "GET",
    });
  }
  function apiUpdateProduct(product) {
    return axios({
      url: `${BASE_URL}/${product.id}`,
      data: product,
      method: "PUT",
    });
  }
